'''
@file lab2.py
'''
import utime
import pyb

class TaskVirtualLED:
    '''
    @brief Turns a virtual LED on and off
    @details Turns a virtual LED on, then off for the interval of time input
    by the user.
    '''
    ## State 0 - Initialization
    s0_init = 0
    ## State 1 - LED off
    s1_led_off = 1
    ## State 2 - LED on
    s2_led_on = 2
    
    def __init__(self, interval, VirtualLED):
        '''
        @brief Creates TaskVirtualLED object
        @param interval Period of time, in seconds, for which LED is on or off
        @param VirtualLED Calling VirtualLEDDriver to be used in task
        '''
        ## State to run on next iteration of task
        self.state = self.s0_init
        ## Number of times the task has run
        self.runs = 0
        ## Time when task starts to run, in milliseconds
        self.start_time = utime.ticks_ms()
        ## The interval of time, in milliseconds, between runs of the task
        self.interval = int(interval*1000)
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        ## LED object, connected to VirtualLEDDriver class
        self.LED = VirtualLED
        
    def run(self):
        '''
        @brief Runs one iteration of TaskVirtualLED
        '''
        ## Shows the time for this run of the task
        self.curr_time = utime.ticks_ms()
        ## Time elapsed since starting task
        self.time_elapse = utime.ticks_diff(self.curr_time, utime.ticks_add(self.interval, self.start_time))/1000
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            
            if (self.state == self.s0_init):
                print(str(self.runs) + ' State 0 Initialization {:0.2f}'.format(self.time_elapse))
                # Run State 0 Code
                self.transitionTo(self.s1_led_off)
                self.LED.off()
        
            elif (self.state == self.s1_led_off):
                print(str(self.runs) + ' State 1 LED off {:0.2f}'.format(self.time_elapse))
                # Run State 1 Code
                self.transitionTo(self.s2_led_on)
                self.LED.on()
                    
            elif (self.state == self.s2_led_on):
                print(str(self.runs) + ' State 2 LED off {:0.2f}'.format(self.time_elapse))
                # Run State 2 Code
                self.transitionTo(self.s1_led_off)
                self.LED.off()
        
            else:
                # Error state
                print(str(self.runs) + ' Error state {:0.2f}'.format(self.time_elapse))
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # Update next time task will run
        
            
    def transitionTo(self, newState):
        '''
        @brief Updates next state to run
        '''
        self.state = newState

class VirtualLEDDriver:
    '''
    @brief A virtual representation of an LED
    '''
    def __init__(self):
        '''
        @brief Creates the VirtualLEDDriver object
        '''
        pass
    def off(self):
        '''
        @brief Turns the virtual LED off
        '''
        print('LED OFF')
    def on(self):
        '''
        @brief Turns the virtual LED on
        '''
        print('LED ON')
        
class TaskPhysicalLED:
    '''
    @brief Varies the brightness of a physical LED with a sawtooth function
    @details Sawtooth function with period input by user, in seconds. Starts at
    0 at 0 seconds, and goes to maximum brightness after 1 period.
    '''
    ## State 0 - Initialization
    s0_init     = 0
    ## State 1 - Zero brightness
    s1_zero     = 1
    ## State 2 - Increase brightness
    s2_bright   = 2
    
    def __init__(self, period, increments, PhysicalLED):
        '''
        @brief Creates an object for TaskPhysicalLED
        @param period Period, in seconds, of sawtooth waveform
        @param increments Number of times within a period to update brightness
        @param PhysicalLED Calling PhysicalLEDDriver to be used in task
        '''
        ## State to run on next iteration of task
        self.state = self.s0_init
        ## Number of times the task has run
        self.runs = 0
        ## Time when task starts to run
        self.start_time = utime.ticks_ms()
        ## Period, in milliseconds, of sawtooth waveform
        self.per = period*1000
        ## Number of times within a period to update brightness
        self.inc = increments
        ## The interval of time, in milliseconds, between runs of the task
        self.interval = int(self.per/self.inc)
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        ## LED object that sets up PhysicalLEDDriver class
        self.LED = PhysicalLED
        ## Increase in percent brightness for each run
        self.perc_inc = 100/self.inc
        ## Current LED percent brightness
        self.perc = 0
        
    def run(self):
        '''
        @brief Runs one iteration of the TaskPhysicalLED
        '''
        ## Shows the time for this run of the task
        self.curr_time = utime.ticks_ms()
        ## Time elapsed since starting task
        self.time_elapse = utime.ticks_diff(self.curr_time, utime.ticks_add(self.interval, self.start_time))/1000
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            
            if (self.state == self.s0_init):
                print(str(self.runs) + ' State 0 Initialization {:0.2f}'.format(self.time_elapse))
                # Run State 0 Code
                self.transitionTo(self.s1_zero)
                self.LED.adjBright(0) # Zero brightness
        
            elif (self.state == self.s1_zero):
                print(str(self.runs) + ' State 1 Zero {:0.2f}'.format(self.time_elapse))
                # Run State 1 Code
                ## Timestamp for when brightness was last zeroed
                self.time_zero = utime.ticks_diff(self.curr_time, self.interval)
                self.perc += self.perc_inc
                self.LED.adjBright(self.perc)
                self.transitionTo(self.s2_bright)
            
            elif (self.state == self.s2_bright):
                print(str(self.runs) + ' State 2 Bright {:0.2f}'.format(self.time_elapse))
                # Run State 2 Code
                ## Time elapsed since brightness last zeroed
                self.time_zero_elapse = utime.ticks_diff(self.curr_time, self.time_zero)
                if (utime.ticks_diff(self.time_zero_elapse, self.per) < 0):
                    # Within sawtooth waveform period
                    self.perc += self.perc_inc # Increase LED brightness by specified increment
                    self.LED.adjBright(self.perc)
                elif (utime.ticks_diff(self.time_zero_elapse, self.per) >= 0):
                    # One sawtooth waveform period has passed
                    self.perc += self.perc_inc
                    self.LED.adjBright(self.perc)
                    self.transitionTo(self.s1_zero)
                    self.perc = 0
                    self.LED.adjBright(0)
                    
            else:
                # Error state
                print(str(self.runs) + ' Error state {:0.2f}'.format(self.time_elapse))
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # Update next time task will run
        
            
    def transitionTo(self, newState):
        '''
        @brief Updates next state to run
        '''
        self.state = newState
        
class PhysicalLEDDriver:
    '''
    @brief This controls the physical LED on the Nucleo
    '''
    def __init__(self, PinNo, TimerNo, ChannelNo):
        '''
        @brief Creates an object for PhysicalLED
        @param PinNo Number of pin, must be board number
        @param TimerNo Timer number
        @param ChannelNo Channel number
        '''
        ## Initialize LED Pin
        self.pin = pyb.Pin(PinNo, pyb.Pin.IN)
        ## Initialize timer
        self.timer = pyb.Timer(TimerNo, freq = 20000)
        ## Channel set to PWM mode
        self.ch = self.timer.channel(ChannelNo, pyb.Timer.PWM, pin=self.pin)
        
    def adjBright(self, perc):
        '''
        @brief Adjusts LED brightness
        @param perc Percent brightness of LED
        '''
        ## Percentage brightness of LED
        self.perc = perc
        self.ch.pulse_width_percent(self.perc)
        print('LED Brightness set to {:} percent'.format(self.perc))