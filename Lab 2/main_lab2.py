'''
@file main_lab2.py
'''

from lab2 import TaskVirtualLED, VirtualLEDDriver, TaskPhysicalLED, PhysicalLEDDriver

## Virtual LED Object
VirtualLED = VirtualLEDDriver()

## Physical LED Object
PhysicalLED = PhysicalLEDDriver('D13', 2, 1)

## Task 1 Object - Virtual LED
task1 = TaskVirtualLED(5, VirtualLED)

## Task 2 Object - Physical LED
task2 = TaskPhysicalLED(10, 10, PhysicalLED)

while True:
    task1.run()
    task2.run()