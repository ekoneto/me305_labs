'''
@file hw0.py

This file is for the ME 416 HW 0 assignment. The code controls an imaginary
elevator.

The user has two buttons to indicate which floor they would like to go to.

The elevator also has sensors to indicate when it has reached each floor.
'''

from random import choice
    
class TaskElevator:
    '''
    @brief A finite state machine to control an elevator
    @details This class implements a finite state machine to control the 
    operation of an elevator.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT           = 0
        
    ## Constant defining State 1
    S1_MOVING_DOWN    = 1
        
    ## Constant defining State 2
    S2_MOVING_UP      = 2
        
    ## Constant defining State 3
    S3_STOPPED_FLOOR1 = 3
        
    ## Constant defining State 4
    S4_STOPPED_FLOOR2 = 4 
    
    def __init__(self, motor, button1, button2, first, second):
        '''
        @brief Creates a TaskElevator object

        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.motor = motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the button_1 object
        self.button1 = button1
        
        ## A class attribute "copy" of the button_2 object
        self.button2 = button2
        
        ## The button object used for the first floor sensor
        self.first = first
        
        ## The button object used for the second floor sensor
        self.second = second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        if (self.state == self.S0_INIT):
            print(str(self.runs) + ' State 0: Initial')
            # Run State 0 Code
            self.transitionTo(self.S1_MOVING_DOWN)
            self.motor.down()
            
        elif (self.state == self.S1_MOVING_DOWN):
            print(str(self.runs) + ' State 1: Moving down')
            # Run State 1 Code
            if self.first.getButtonState():
                self.transitionTo(self.S3_STOPPED_FLOOR1)
                self.motor.off()
        
        elif (self.state == self.S2_MOVING_UP):
            print(str(self.runs) + ' State 2: Moving up')
            # Run State 2 Code
            if self.second.getButtonState():
                self.transitionTo(self.S4_STOPPED_FLOOR2)
                self.motor.off()
            
        elif (self.state == self.S3_STOPPED_FLOOR1):
            print(str(self.runs) + ' State 3: Stopped on Floor 1')
            # Run State 3 Code
            if self.button2.getButtonState():
                self.transitionTo(self.S2_MOVING_UP)
                self.motor.up()
            
        elif (self.state == self.S4_STOPPED_FLOOR2):
            print(str(self.runs) + ' State 4: Stopped on Floor 2')
            # Run State 4 Code
            if self.button1.getButtonState():
                self.transitionTo(self.S1_MOVING_DOWN)
                self.motor.down()
                
        else:
            # Error state
            pass
        
        self.runs += 1
        
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A button class
    @details    This class represents a button that the can be pushed by the
                user to indicate that they would like to ride the elevator to
                a different floor. It also represents the sensors that indicate
                when the elevator has reached the desired floor. As of right 
                now we will pretend to use hardware to implement this class.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized value. 1 indicates that the button is
                    pushed. 0 clears the button.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to move the elevator
                up and down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def up(self):
        '''
        @brief Moves the motor such that the elevator goes up
        '''
        print('Motor moving up')
    
    def down(self):
        '''
        @brief Moves the motor such that the elevator goes down
        '''
        print('Motor moving down')
    
    def off(self):
        '''
        @brief Stops the motor
        '''
        print('Motor Stopped')