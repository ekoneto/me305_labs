'''
@file main_hw0.py
'''

from hw0 import Button, MotorDriver, TaskElevator
        
## Motor Object
motor = MotorDriver()

## Button Object for Floor 1 button "button1"
button1 = Button('1')

## Button Object for Floor 2 button "button2"
button2 = Button('2')

## Button Object for Floor 1 sensor "first"
first = Button('3')

## Button Object for Floor 2 sensor "second"
second = Button('4')

## Task object
task1 = TaskElevator(motor, button1, button2, first, second) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(50): #Will change to   "while True:" once we're on hardware
    task1.run()