'''
@file lab4_user.py
@brief This file contains the TaskUser for the user interface task of Lab 4
@author Emma Oneto
@date November 17, 2020
'''
import serial
import time
import csv
import matplotlib.pyplot as plt

class TaskUser:
    '''
    @brief FSM that allows user to collect & view data from the encoder
    @details This finite state machine allows the user to collect and view
    data from the encoder. When the user inputs G, the task starts collecting
    encoder position data. When the user inputs S or after a specified time
    period, whichever comes first, the task stops collecting data, generates a 
    plot of encoder position over time, and creates a .CSV file with the time 
    and encoder position data.
    '''
    ## State 0: Initialization
    S0_init         = 0
    ## State 1: Wait for user to start data collection
    S1_wait_start   = 1
    ## State 2: Wait for user to end data collection
    S2_wait_end     = 2
    ## State 3: Do nothing, task over
    S3_nothing      = 3
    
    def __init__ (self, dbg = True):
        '''
        @brief Creates TaskUser object
        @param dbg Boolean indicating if task should print debugging statements
        '''
        ## Serial port
        self.ser = serial.Serial(port='COM3',baudrate=115200,timeout=None)
        ## Option to print debugging statements as code runs
        self.dbg = dbg
        ## State upon starting the task
        self.state = self.S0_init
        ## Number of times task has run
        self.runs = 0
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        if (self.state == self.S0_init):
            self.printTrace()
            # Run State 0 code
            print('Type G to start encoder position data collection')
            self.transitionTo(self.S1_wait_start)
        
        elif (self.state == self.S1_wait_start):
            self.printTrace()
            # Run State 1 code
            ## Character typed by user
            self.cmd = input()
            if self.cmd:
                if (self.cmd == 'G'):
                    self.ser.write(str(self.cmd).encode('ascii'))
                    self.transitionTo(self.S2_wait_end)
                    ## Start time for data collection, in milliseconds
                    self.start_time = time.time()
                    ## Array containing encoder position values
                    self.posArray = []
                    ## Array containing time values
                    self.timeArray = []
                    if self.dbg:
                        print('{:} sent to the Nucleo'.format(self.cmd))
                else:
                    print('Invalid input: Type G to start encoder position data collection')
                self.cmd = None
        
        elif (self.state == self.S2_wait_end):
            self.printTrace()
            # Run State 2 code
            ## Current time, in milliseconds
            self.curr_time = time.time()
            ## Time elapsed since data collection started, in milliseconds
            self.time_elapse = self.curr_time - self.start_time
            self.cmd = input()
            if self.cmd:
                if (self.cmd == 'S'):
                    self.ser.write(str(self.cmd).encode('ascii'))
                    # Create CSV and plot
                    with open('lab4data.csv', 'w', newline = '') as f:
                        ## CSV file writer object
                        self.csv = csv.writer(f)
                        for n in range (len(self.timeArray)):
                            self.csv.writerow([self.timeArray[n], self.posArray[n]])
                    plt.plot(self.timeArray, self.posArray)
                    self.transitionTo(self.S3_nothing)
                else:
                    print('Invalid input: Type S to terminate encoder position data collection')
            elif (self.time_elapse <= 10000):
                # Receive data from TaskData
                ## String of time & position data received from TaskData
                self.array_str = self.ser.readline()
                ## List of time & position data from TaskData
                self.array_list = self.array_str.strip().split(',')
                self.timeArray.append(int(self.array_list[0])) # Add newest time value to time array
                self.posArray.append(int(self.array_list[1])) # Add newest position value to position array
                time.sleep(0.2)
            elif (self.time_elapse >= 10000):
                # Create CSV and plot
                with open('lab4data.csv', 'w', newline = '') as f:
                    ## CSV file writer object
                    self.csv = csv.writer(f)
                    for n in range (len(self.timeArray)):
                        self.csv.writerow([self.timeArray[n], self.posArray[n]])
                plt.plot(self.timeArray, self.posArray)
                self.transitionTo(self.S3_nothing)
            else:
                # Error state
                print('Error: TaskUser: run: State 2')
        
        elif (self.state == self.S3_nothing):
            self.printTrace()
            self.ser.close()
        
        else:
            # Error state
            pass
        
        self.runs += 1
        
    def transitionTo(self, newState):
        '''
        @brief Transitions from current state to new state
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        @brief Prints a debugging statement
        '''
        if self.dbg:
            print('TaskUser / Run: {:} / State: {:}'.format(self.runs, self.state))
            
if __name__ == '__main__':
    while True:
        ## Task User object
        task2 = TaskUser(dbg = True)
        task2.run()