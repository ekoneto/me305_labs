'''
@file main.py
@brief This file runs the Lab 4 tasks at the same time
@author Emma Oneto
@date November 17, 2020
'''

from lab4_data import TaskData, EncoderDriver
# from lab4_user import TaskUser

## Encoder Driver object
Encoder = EncoderDriver()

## Task Data
task1 = TaskData(Encoder, dbg = True)

# ## Task User
# task2 = TaskUser(dbg = True)

while True:
    task1.run()
    # task2.run()

# from pyb import UART

# myuart = UART(2)

# while True:
#     if myuart.any() != 0:
#         val = myuart.readchar()
#         myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')