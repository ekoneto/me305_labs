'''
@file lab4_data.py
@brief This file contains the TaskData for the data collection task of Lab 4
@author Emma Oneto
@date November 17, 2020
'''
import utime
import pyb
from pyb import UART

class TaskData:
    '''
    @brief FSM that collects encoder position data when prompted by the user
    @details This finite state machine collects encoder position data and
    sends it to TaskUser. When the user inputs G, the task starts collecting
    encoder position data at a specified frequency. The task stores the data
    on the Nucleo for batch transmission to a PC. When the user inputs S or
    after a time period specified by the user, the task stops collecting data
    and sends encoder position and time data to TaskUser.
    '''
    ## State 0: Initialization
    S0_init         = 0
    ## State 1: Wait for user to start data collection
    S1_wait_start   = 1
    ## State 2: Wait for user to end data collection
    S2_wait_end     = 2
    ## State 3: Do nothing, task over
    S3_nothing      = 3
    
    def __init__ (self, Encoder, dbg = True):
        '''
        @brief Creates TaskData object
        @param dbg Boolean indicating if task should print debugging statements
        @param Encoder object, connected to EncoderDriver
        '''
        ## Serial port
        self.myuart = UART(2)
        ## Option to print debugging statements as code runs
        self.dbg = dbg
        ## State upon starting the task
        self.state = self.S0_init
        ## Number of times task has run
        self.runs = 0
        ## Encoder object, connected to EncoderDriver
        self.Encoder = Encoder
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        if (self.state == self.S0_init):
            self.printTrace()
            # Run State 0 code
            self.transitionTo(self.S1_wait_start)
        
        elif (self.state == self.S1_wait_start):
            self.printTrace()
            # Run State 1 code
            if self.myuart.any():
                ## ASCII decimal value of character input by user
                self.cmd = self.myuart.readchar()
                if (self.cmd == 71): # cmd = G
                    self.transitionTo(self.S2_wait_end)
                    ## Start time for data collection, in milliseconds
                    self.start_time = utime.ticks_ms()
                    self.cmd = None
                else:
                    self.cmd = None
        
        elif (self.state == self.S2_wait_end):
            self.printTrace()
            # Run State 2 code
            ## Current time, in milliseconds
            self.curr_time = utime.ticks_ms()
            ## Time elapsed since data collection started, in milliseconds
            self.time_elapse = utime.ticks.diff(self.curr_time,self.start_time)
            if (self.time_elapse <= 10000): # If 10 seconds have not passed yet
                self.Encoder.collectPos()
                ## Array containing time (ms) and position (ticks)
                array = "{:}, {:} \n".format(self.time_elapse, self.position)
                return array
                utime.sleep_ms(200)
            elif self.myuart.any():
                self.cmd = self.myuart.readchar()
                if (self.cmd == 83): # S has been entered
                    self.transitionTo(self.S3_nothing)
                else: # Something else has been entered
                    self.cmd = None
            elif (self.time_elapse >= 10000): # 10 seconds have passed
                self.transitionTo(self.S3_nothing)
            else:
                # Error state
                print('Error: TaskData.run.State 2')
        
        elif (self.state == self.S3_nothing):
            self.printTrace()
            # Run State 3 code
        
        else:
            # Error state
            pass
        
        self.runs += 1
        
    def transitionTo(self, newState):
        '''
        @brief Transitions from current state to new state
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        @brief Prints a debugging statement
        '''
        if self.dbg:
            print('TaskData / Run: {:} / State: {:}'.format(self.runs, self.state))

class EncoderDriver:
    '''
    @brief Samples position from encoder
    @details Samples position from encoder at 5 Hz for up to 10 seconds,
    unless terminated by the user.
    '''
    def __init__(self):
        '''
        @brief Creates EncoderDriver object
        '''
        ## Initialize timer
        self.timer = pyb.Timer(3, prescaler = 0, period = 65535)
        ## Initialize pin 1 of encoder counter
        self.pin1 = pyb.Pin.cpu.A6
        ## Initialize pin 2 of encoder counter
        self.pin2 = pyb.Pin.cpu.A7
        ## Channel 1 set to encoder mode
        self.ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin=self.pin1)
        ## Channel 2 set to encoder mode
        self.ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin=self.pin2)
        
        ## Array containing last two encoder position values
        self.count = [0]
        ## Encoder position, corrected for bad deltas
        self.position = 0
    
    def collectPos(self):
        '''
        @brief Collects encoder position data
        '''
        self.count.append(self.timer.counter())
        
        ## Delta between new count value and previous count value
        self.delta = self.count[1] - self.count[0]
        
        if (self.delta < self.per/2): # Good delta
            self.position += self.delta # Add good delta to encoder position
            
        elif (self.delta >= self.per/2): # Bad delta
        
            if (self.delta < 0): # Bad delta is negative
                self.delta += self.per # Add period
                self.position += self.delta # Add corrected delta to encoder position
                
            elif (self.delta >= 0):  # Bad delta is positive
                self.delta -= self.per # Subtract period
                self.position += self.delta # Add corrected delta to encoder position
        else:
            # Error state
            print('Error: EncoderDriver update()')
        
        self.count.pop(0) # Remove first value of count array
        return self.position