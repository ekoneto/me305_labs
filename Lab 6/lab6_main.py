'''
@file lab6_main.py
@brief Runs Lab 6 code to control motor speed
@author Emma Oneto
@date December 3, 2020
'''
from lab6 import TaskMotor, ClosedLoop, MotorDriver, EncoderDriver

## Controller object
Controller = ClosedLoop()

## Motor driver object
Motor = MotorDriver()

## Encoder driver object
Encoder = EncoderDriver()

## Task - Runs through closed loop system
task = TaskMotor(Controller, Motor, Encoder)

while True:
    task.run()