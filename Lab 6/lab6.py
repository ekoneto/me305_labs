'''
@file lab6.py
@brief Source code for Lab 6: DC Motor Control
@author Emma Oneto
@date December 3, 2020
'''
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import time

class TaskMotor:
    '''
    @brief Plots controlled DC motor speed
    @details Controls motor speed using a closed loop with proportional
    controller. User inputs proportional controller gain and target motor
    speed. Plots motor speed over time period and at time intervals input by 
    user. After each simulation, user can choose to run another or end program
    '''
    ## State 0: Initialization
    S0_init         = 0
    ## State 1: Wait for Kp input
    S1_wait_Kp      = 1
    ## State 2: Wait for Wref input
    S2_wait_Wref    = 2
    ## State 3: Wait for time input
    S3_wait_time    = 3
    ## State 4: Wait for interval input
    S4_wait_int     = 4
    ## State 5: Control loop
    S5_loop         = 5
    ## State 6: Wait to continue, Y/N
    S6_wait_cont    = 6
    ## State 7: Do nothing
    S7_stop         = 7
    
    def __init__(self, Controller, Motor, Encoder):
        '''
        @brief Creates TaskMotor object
        @param Controller Controller object to run from ClosedLoop class
        @param Motor Motor object to run from MotorDriver class
        @param Encoder Encoder object to run from EncoderDriver class
        '''
        #print('Creating a task object')
        
        ## Controller object - used to run ClosedLoop class
        self.Control = Controller        
        ## Motor object - used to run MotorDriver class
        self.Motor = Motor        
        ## Encoder object - used to run EncoderDriver class
        self.Encoder = Encoder
        
        ## Initial state
        self.state = self.S0_init
        ## Initial motor speed, rad/s
        self.Wmeas = 0        
        ## Initial Kp value, %-s/rad
        self.Kp = 0        
        ## Initial reference motor speed, rad/s
        self.Wref = 0        
        ## Initial duty cycle, %
        self.duty = 0        
        ## Initial motor voltage, V
        self.Vm = 0
        ## Initial time duration to run closed loop, milliseconds
        self.tend = 0
        ## Initial time interval between closed loop runs, milliseconds
        self.tint = 0
        
        ## Array containing motor voltage values, V
        self.VmArray = np.zeros(1)
        ## Array containing measured motor speed values, rad/s
        self.WmArray = np.zeros(1)        
        ## Array containing time values, milliseconds
        self.tArray = np.zeros(1)
        
    def run(self):
        '''
        @brief Controls motor speed based on user input parameters
        @details Controls motor speed based on user inputs for proportional
        controller gain and target motor speed. Plots motor speed over time
        period and at time intervals input by user
        '''
        if (self.state == self.S0_init):
            # Run State 0 code
            self.transitionTo(self.S1_wait_Kp)
        
        elif (self.state == self.S1_wait_Kp):
            # Run State 1 code
            self.Kp = input('Enter a value for the controller gain, Kp: ')
            if self.testval(self.Kp):
                self.Kp = float(self.Kp)
                print('Kp = {:} %-s/rad'.format(self.Kp))
                self.transitionTo(self.S2_wait_Wref)
                           
        elif (self.state == self.S2_wait_Wref):
            # Run State 2 code
            self.Wref = input('Enter a value for the motor speed in rad/s, Wref: ')
            if self.testval(self.Wref):
                self.Wref = float(self.Wref)
                print('Wref = {:} rad/s'.format(self.Wref))
                self.transitionTo(self.S3_wait_time)
                
        elif (self.state == self.S3_wait_time):
            # Run State 3 code
            self.tend = input('Enter a value for the time, in milliseconds: ')
            if self.testval(self.tend):
                self.tend = float(self.tend)
                print('Time = {:} ms'.format(self.tend))
                self.transitionTo(self.S4_wait_int)
                
        elif (self.state == self.S4_wait_int):
            # Run State 4 code
            self.tint = input('Enter a value for the interval, in milliseconds: ')
            if self.testval(self.tint):
                self.tint = float(self.tint)
                print('Interval = {:} ms'.format(self.tint))
                ## Start time for running closed loop, milliseconds
                self.tstart = time.time()
                self.transitionTo(self.S5_loop)
        
        elif (self.state == self.S5_loop):
            # Run State 5 code
            ## Time elapsed in closed loop, milliseconds
            self.t = time.time() - self.tstart
            if (self.t <= self.tend):
                self.duty = self.Control.update(self.Wref, self.Wmeas, self.Kp)
                self.Vm = (self.Motor.setDuty(self.duty))    
                self.VmArray = np.append(self.VmArray, self.Vm)
                self.tArray = np.append(self.tArray, self.t)
                ## Transfer function output: motor speed arrray, rad/s
                self.yout = self.Encoder.speed(self.VmArray, self.tArray)
                self.Wmeas = self.yout[-1]
                self.WmArray = np.append(self.WmArray, self.Wmeas)
                #print('T {:0.2f} | L {:0.2f} | Vm {:0.2f} | Wm {:0.2f} | Wr {:}'.format(self.t, self.duty, self.Vm, self.Wmeas, self.Wref))
                time.sleep(self.tint/1000)
            elif (self.t >= self.tend):
                plt.plot(self.tArray, self.WmArray)
                plt.xlabel('Time [ms]')
                plt.ylabel('Motor speed [rad/s]')
                plt.show()
                self.transitionTo(self.S6_wait_cont)
            else:
                # Error state
                print('Error: TaskMotor.run(): Running in closed loop')
        
        elif (self.state == self.S6_wait_cont):
            # Run State 4 code
            ## User input to continue or end program
            self.cont = input('Continue? Enter Y or N: ')
            if self.cont:
                if (self.cont == 'Y'):
                    self.transitionTo(self.S1_wait_Kp)
                    self.Wmeas = 0
                    self.duty = 0
                    self.Vm = 0
                    self.VmArray = np.zeros(1)
                    self.WmArray = np.zeros(1)
                    self.tArray = np.zeros(1)
                elif (self.cont == 'N'):
                    print('Program ended')
                    self.transitionTo(self.S7_stop)
                else:
                    print('Invalid input. Please enter "Y" for yes or "N" for no')
                    self.cont = None
                    pass
        
        elif (self.state == self.S7_stop):
            # Run State 5 code
            pass
        
        else:
            # Error state
            pass
        
    def transitionTo(self, newState):
        '''
        @brief Updates next state to run
        @param newState New state to transition to
        '''
        self.state = newState
        
    def testval(self, val):
        '''
        @brief Checks if user input a number
        @details Checks if user input a number, integer or float, for Kp. If a
        number was not entered, function erases the user input and asks the 
        user to input a number. If a number was entered, function returns the 
        number as a string.
        @param val User input
        '''
        try:
            float(val)
        except:
            print('Invalid input: Please enter a number')
            val = None
        else:
            return val
        
class ClosedLoop:
    '''
    @brief Proportional controller
    @details Proportional controller that adjust motor voltage based on error
    between desired motor speed and measured motor speed
    '''
    def __init__(self):
        '''
        @brief Create ClosedLoop object
        '''
        #print('Creating a controller object')
        
    def update(self, Wref, Wmeas, Kp):
        '''
        @brief Returns duty cycle based on the measured and reference values
        @param Wref Reference motor speed, rad/s
        @param Wmeas Measured motor speed, rad/s
        @param Kp Proportional gain, %-s/rad
        '''
        ## Reference motor speed, rad/s
        self.Wref = Wref
        
        ## Measured motor speed, rad/s
        self.Wmeas = Wmeas
        
        ## Proportional gain, %-s/rad
        self.Kp = Kp
        
        ## New duty cycle, %
        duty = self.Kp*(self.Wref - self.Wmeas)
        return duty
        
class MotorDriver:
    '''
    @brief Sets motor voltage
    @details Sets voltage sent to virtual motor by adjusting the duty cycle
    '''
    def __init__ (self):
        '''
        @brief Creates MotorDriver object
        '''
        #print('Creating a motor object')
        
        ## Motor nominal DC voltage, V
        self.Vdc = 18
        
        ## Initial motor voltage, V
        self.Vm = 0
        
        ## Initial level, duty cylce
        self.L = 0
        
    def setDuty(self, duty):
        '''
        @brief Sets the duty cycle of motor
        @details This method sets the duty cycle to be sent to the motor to the
        given level. Positive values cause rotation in the forward direction,
        negative values in the reverse direction.
        @param duty Duty cycle of PWM signal sent to motor, %
        '''
        self.L = duty/100
        
        if (self.L >= 1):
            self.Vm = self.Vdc
            return self.Vm
        elif (self.L <= -1):
            self.Vm = -self.Vdc
            return self.Vm
        elif (self.L > -1 and self.L < 1):
            self.Vm = self.L*self.Vdc
            return self.Vm
        else:
            # Error state
            print('Error: setDuty')
        
class EncoderDriver:
    '''
    @brief Simulates encoder behavior
    @details Simulates encoder behavior by "meaasuring" motor speed. Contains
    the motor parameters and uses them in a transfer function that outputs
    motor speed based on voltage supplied to the motor. Positive output for
    speed indicates rotation in the forward direction. Negative output for
    speed indicates rotation in the reverse direction.
    '''
    def __init__(self):
        '''
        @brief Creates EncoderDriver object
        '''
        #print('Creating an Encoder object')
        
        ## Torque constant, N-m/A
        self.Kt = 0.0138
        
        ## Speed constant, V/(rad/s)
        self.Kv = 0.00144
        
        ## Rotational moment of inertia, kg-m2
        self.J = 0.000000522
        
        ## Resistance, Ohm
        self.R = 2.21
        
        ## Viscous damping coefficient, N-m/(rad/s)
        self.b = 0.0000860
    
    def speed(self, U, T):
        '''
        @brief Reads motor speed
        @param U Input array: motor voltage, V
        @param T Input time array, milliseconds
        '''
        # Create transfer function, input motor voltage & output motor speed
        
        ## Speed transfer function numerator
        self.num = [self.Kt/self.R]
        
        ## Speed transfer function denominator
        self.den = [self.J, self.b + self.Kv*self.Kt/self.R]
        
        ## Speed transfer function
        self.sys = signal.TransferFunction(self.num, self.den)
        
        # Time response
        
        ## Input array: motor voltage
        self.U = U      
        ## Input time array, seconds
        self.T = np.divide(T, 1000)
        
        ## Output: time array, motor speed array [rad/s]
        t,y,x = signal.lsim(self.sys, self.U, self.T)
        return y
        
    # def position(self):
    #     '''
    #     @brief Reads motor position
    #     '''
    #     # Create transfer function, input motor voltage & output motor position
        
    #     ## Position transfer function numerator
    #     self.num = [self.Kt/self.R]
        
    #     ## Position transfer function denominator
    #     self.den = [self.J, self.b + self.Kv*self.Kt/self.R, 0]
        
    #     ## Position transfer function
    #     self.sys = signal.TransferFunction(self.num, self.den)
        
    #     # Time response
        
    #     ## Input array: motor voltage
    #     self.U = np.full(100, 18)        
    #     ## Input time array, seconds
    #     self.T = np.linspace(0, 0.05, 100)
        
    #     ## Output: time array, motor position array [rad], some other array
    #     t,y,x = signal.lsim(self.sys, self.U, self.T)
    #     return y
    
# if __name__ == '__main__':
#     # print(Motor.setDuty(50))
#     # print(Encoder.speed())
#     Controller = ClosedLoop()
#     Motor = MotorDriver()
#     Encoder = EncoderDriver()
#     task = TaskMotor(Controller, Motor, Encoder)
#     while True:
#         task.run()