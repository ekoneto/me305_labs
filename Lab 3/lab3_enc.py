'''
@file lab3_enc.py
@brief This file contains the EncoderTask and EncoderDriver for Task 1 of lab 3.
@author Emma Oneto
@date October 28, 2020
'''

import utime
import pyb
import lab3_shares as shares

class TaskEncoder:
    '''
    @brief   FSM that gets position & delta and sets position of an encoder
    @details A finite state machine that uses encoder ticks to keep track of
            the position of an encoder. This task sends current encoder
            position and the delta between the current and previous encoder 
            position reading to the user interface task, lab3_user. The task
            can also reset the encoder position based on input from the user
            task.
    '''
    ## State 0: Initialization
    S0_init             = 0
    ## State 1: Waiting for next encoder position update time
    S1_wait_for_update  = 1
    
    def __init__(self, Encoder, cpr, w, dbg = False):
        '''
        @brief Creates the TaskEncoder object
        @param cpr Number of pole pairs in the motor
        @param w Motor speed in rpm
        @param dbg Boolean indicating if task should print debugging statements
        '''
        ## State upon starting the task
        self.state = self.S0_init
        
        ## Option to print debugging statements as code runs
        self.dbg = dbg
        
        ## Number of pole pairs for motor
        self.cpr = cpr # User inputs
        
        ## Motor speed in rpm
        self.w = w
        
        ## Pulses per second
        self.pps = self.cpr*4*self.w/60
        
        ## Interval of time between updates, in milliseconds, for 16-bit timer
        self.interval = int(1000*32768/self.pps)
        
        ## Time when task starts running
        self.start_time = utime.ticks_ms()
        
        ## Next time to update
        self.update_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Number of times task has run
        self.runs = 0
        
        ## Encoder object, connected to EncoderDriver
        self.Encoder = Encoder
        
    def run(self):
        '''
        @brief Runs one iteration of TaskEncoder
        '''
        ## Updates current time
        self.curr_time = utime.ticks_ms()
        
        if (utime.ticks_diff(self.curr_time, self.update_time) >= 0):
            if (self.state == self.S0_init):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_wait_for_update)
            elif (self.state == self.S1_wait_for_update):
                self.printTrace()
                # Run State 1 Code
                # Checking if current time is greater than next "scheduled" update time
                self.Encoder.update()
            else:
                # Error state
                pass

        self.runs += 1
        self.update_time = utime.ticks_add(self.update_time, self.interval)
            
    def transitionTo(self, newState):
        '''
        @brief Transitions from current state to new state
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        @brief Prints a debugging statement
        '''
        if self.dbg:
            print('TaskEncoder / Run: {:} / State: {:} / Time: {:}'.format(self.runs, self.state, utime.ticks_diff(self.curr_time, self.start_time)))
    
class EncoderDriver:
    '''
    @brief Gets encoder position from encoder
    '''
    def __init__(self, TimerNo, PS, per, ch1, ch2, pin1, pin2, dbg = False):
        '''
        @brief Creates EncoderDriver object
        @param TimerNo Number of timer on pyboard
        @param PS Timer prescalar value
        @param per Timer period. Counter value at which the counter resets
        @param ch1 Number for channel 1 pin
        @param ch2 Number for channel 2 pin
        @param pin1 Pin used for channel 1 of the timer, must be board name
        @param pin2 Pin used for channel 2 of the timer, must be board name
        '''        
        ## Option to print debugging statements as code runs
        self.dbg = dbg
        
        ## Array containing two latet raw count values
        self.count = [0]
        
        ## Encoder position, corrected for bad deltas
        self.position = 0
        
        ## Encoder position array containing last two values for position
        self.posArray = [0, 0]
        
        ## Encoder timer period
        self.per = per
        
        ## Initialize timer
        self.timer = pyb.Timer(TimerNo, prescaler = PS, period = per)
        
        ## Initialize pin 1 of encoder counter
        self.pin1 = pyb.Pin(pin1, pyb.Pin.IN)
        
        ## Initialize pin 2 of encoder counter
        self.pin2 = pyb.Pin(pin2, pyb.Pin.IN)
        
        ## Channel 1 set to encoder mode
        self.ch1 = self.timer.channel(ch1, pyb.Timer.ENC_AB, pin=self.pin1)
        
        ## Channel 2 set to encoder mode
        self.ch2 = self.timer.channel(ch2, pyb.Timer.ENC_AB, pin=self.pin2)

    def run(self):
        '''
        @brief Runs EncoderDriver function based on user input from TaskUser
        '''
        if shares.cmd:            
            if (shares.cmd == 122): # User input z
                self.setPosition(0)
                shares.resp = str('Encoder position has been zeroed')
            elif (shares.cmd == 112): # User input p
                shares.resp = str(self.getPosition())
            elif (shares.cmd == 100): # User input d
                self.getDelta()
                shares.resp = str(self.getDelta())
            else:
                shares.resp = str('Input error: Please enter z, p, or d')
            shares.cmd = None
    
    def update(self):
        '''
        @brief Updates the encoder position
        '''
        self.count.append(self.timer.counter())
        
        ## Delta between new count value and previous count value
        self.delta = self.count[1] - self.count[0]
        
        if (self.delta < self.per/2): # Good delta
            self.position += self.delta # Add good delta to encoder position
            
        elif (self.delta >= self.per/2): # Bad delta
        
            if (self.delta < 0): # Bad delta is negative
                self.delta += self.per # Add period
                self.position += self.delta # Add corrected delta to encoder position
                
            elif (self.delta >= 0):  # Bad delta is positive
                self.delta -= self.per # Subtract period
                self.position += self.delta # Add corrected delta to encoder position
        else:
            # Error state
            print('Error: EncoderDriver update()')
            pass

        self.count.pop(0) # Remove first value of count array
        self.posArray.pop(0) # Remove first value of position array
        self.posArray.append(self.position) # Add newest position value to position array
        
        if self.dbg:
            print('Count: {:} / Delta: {:} / Position: {:} / Position Array: {:}'.format(self.count, self.delta, self.position, self.posArray))
            
    def getPosition(self):
        '''
        @brief Returns most recent encoder position update
        '''
        strPosition = 'Encoder position: {:}'.format(self.position)
        return strPosition
        
    def setPosition(self, newPosition):
        '''
        @brief Sets latest position call to specified value
        @param newPosition Value for latest position call as set by user
        '''
        self.position = newPosition
        self.posArray.pop(0) # Remove first value of position array
        self.posArray.append(self.position) # Add newest position value to position array
        
    def getDelta(self):
        '''
        @brief Gets delta between previous and current encoder positions
        '''
        ## Difference between latest and previous position calls
        self.posDelta = self.posArray[1] - self.posArray[0]
        strDelta = 'Position Delta: {:}'.format(self.posDelta)
        return strDelta