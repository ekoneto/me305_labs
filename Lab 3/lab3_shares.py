'''
@file lab3_shares.py
@brief This file contains inter-task variables for Lab 3
@author Emma Oneto
@date October 28, 2020
'''

## User input from TaskUser, ASCII integer
cmd = None

## EncoderDriver response, string
resp = None