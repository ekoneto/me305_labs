'''
@file lab3_user.py
@brief This file contains the TaskUser for Task 2 of Lab 3
@author Emma Oneto
@date October 28, 2020
'''
import utime
from pyb import UART
import lab3_shares as shares

class TaskUser:
    '''
    @brief FSM that allows user to interact with the encoder
    @details This finite state machine responds to user inputs with information
    about the encoder position. When the user inputs z, the encoder position
    zeros. When the user inputs p, the task returns the encoder position. For
    an input d, the task returns the delta between the previous and current
    enocder position.
    '''
    ## State 0: Initialization
    S0_init         = 0
    
    ## State 1: Wait for user input
    S1_wait_cmd     = 1
    
    ## State 2: Wait for EncoderDriver response
    S2_wait_resp    = 2
    
    def __init__(self, Encoder, cpr, w, dbg = False):
        '''
        @brief Creates TaskUser object
        @param cpr Number of pole pairs in the motor
        @param w Motor speed in rpm
        @param dbg Boolean indicating if task should print debugging statements
        '''
        ## Serial port
        self.ser = UART(2)
        
        ## Option to print debugging statements as code runs
        self.dbg = dbg
        
        ## State upon starting the task
        self.state = self.S0_init
        
        ## Number of times task has run
        self.runs = 0
        
        ## Number of pole pairs for motor
        self.cpr = cpr # User inputs
        
        ## Motor speed in rpm
        self.w = w
        
        ## Pulses per second
        self.pps = self.cpr*4*self.w/60
        
        ## Interval of time between updates, in milliseconds
        self.interval = int(1000*32768/self.pps)
        
        ## Time when task starts running
        self.start_time = utime.ticks_ms()
        
        ## Next time to update
        self.update_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Encoder object, connected to EncoderDriver
        self.Encoder = Encoder
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## Updates current time
        self.curr_time = utime.ticks_ms()
        
        if (utime.ticks_diff(self.curr_time, self.update_time) >= 0):
            if (self.state == self.S0_init):
                self.printTrace()
                # Run State 0 Code
                print('Type z to zero encoder position', 'Type p to return encoder position', 'Type d to return encoder position delta', sep='\n')
                self.transitionTo(self.S1_wait_cmd)
            elif (self.state == self.S1_wait_cmd):
                self.printTrace()
                # Run State 1 Code
                if self.ser.any():
                    shares.cmd = self.ser.readchar()
                    self.Encoder.run()
                    self.transitionTo(self.S2_wait_resp)
            elif (self.state == self.S2_wait_resp):
                self.printTrace()
                # Run State 2 Code
                if shares.resp:
                    print(shares.resp)
                    shares.resp = None
                    self.transitionTo(self.S1_wait_cmd)
            else:
                # Error State
                print('Error: Please enter z, p, or d')
        
        self.runs += 1
        self.update_time = utime.ticks_add(self.update_time, self.interval)
                
    def transitionTo(self, newState):
        '''
        @brief Transitions from current state to new state
        '''
        self.state = newState
    
    def printTrace(self):
        '''
        @brief Prints a debugging statement
        '''
        if self.dbg:
            print('TaskUser / Run: {:} / State: {:} / Time: {:}'.format(self.runs, self.state, utime.ticks_add(self.curr_time, -self.start_time)))