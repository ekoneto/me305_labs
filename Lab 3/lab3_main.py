'''
@file lab3_main.py
@brief This file runs the Lab 3 tasks at the same time
@author Emma Oneto
@date October 28, 2020
'''

from lab3_enc import TaskEncoder, EncoderDriver
from lab3_user import TaskUser

## Encoder driver object
# Input order: timerNo, PS, per, ch1, ch2, pin1, pin2
Encoder = EncoderDriver(3, 0, 65535, 1, 2, 'D12', 'D11', dbg = False)

## Task 1: Updates encoder position call
# Input order: driver, cpr, w, dbg = True
task1 = TaskEncoder (Encoder, 7, 310, dbg = False)

## Task 2: User interface task
# Input order: cpr, w, dbg = True
task2 = TaskUser(Encoder, 7, 310, dbg = False)

while True:
    task1.run()
    task2.run()