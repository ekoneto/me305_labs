'''
@file lab7_main.py
@brief Runs Lab 7 code: reference tracking with DC motor speed & position
@author Emma Oneto
@date December 3, 2020
'''

from lab7 import TaskTrack, ClosedLoop, MotorDriver, EncoderDriver

## Controller object
Controller = ClosedLoop()

## Motor driver object
Motor = MotorDriver()

## Encoder driver object
Encoder = EncoderDriver()

## Task - Reference tracks motor speed and position
task = TaskTrack(Controller, Motor, Encoder)

while True:
    task.run()