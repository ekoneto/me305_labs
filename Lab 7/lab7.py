'''
@file lab7.py
@brief Source code for Lab 7: DC Motor Reference Tracking
@author Emma Oneto
@date December 3, 2020
'''
from scipy import signal
import math
import matplotlib.pyplot as plt
import numpy as np

class TaskTrack:
    '''
    @brief Reference tracking for DC motor speed and position
    @details Reference tracking for DC motor speed and position. Follows
    values provided in reference.csv file. Uses two proportional controllers,
    one based on speed error and the other on position error. Plots reference
    and measured velocity and position time response. Also provides J, a
    performance measurement based on error between reference and measured
    values.
    '''
    ## State 0: Initialization
    S0_init     = 0
    ## State 1: Wait for speed Kp input from user
    S1_KpW      = 1
    ## State 2: Run closed loop
    S2_loop     = 2
    ## State 3: Wait for Y(yes)/N(no) input from user to continue
    S3_cont     = 3
    ## State 4: Program ended, do nothing
    S4_end      = 4
    ## State 5: Wait for position Kp input from user
    S5_KpPos    = 5
    
    def __init__(self, Controller, Motor, Encoder):
        '''
        @brief Creates TaskTrack object
        @param Controller Controller object to run from ClosedLoop class
        @param Motor Motor object to run from MotorDriver class
        @param Encoder Encoder object to run from EncoderDriver class
        '''
        ## Controller object - used to run ClosedLoop class
        self.Control = Controller        
        ## Motor object - used to run MotorDriver class
        self.Motor = Motor        
        ## Encoder object - used to run EncoderDriver class
        self.Encoder = Encoder
        
        # Get reference time, speed, and position arrays from csv file
        ## Time array from csv file, seconds
        self.TrefArray = []
        ## Reference speed array, rad/s
        self.WrefArray = []
        ## Reference speed array, rpm
        self.WrefArray_rpm = []
        ## Reference position array, rad
        self.PrefArray = []
        ## Reference position array, deg
        self.PrefArray_deg = []
        ref = open('reference.csv');
        while True:
            line = ref.readline()
            if line == '':
                break
            else:
                (t,v,x) = line.strip().split(',');
                self.TrefArray.append(float(t))
                self.WrefArray.append(float(v)*math.pi/30) # rad/s
                self.WrefArray_rpm.append(float(v)) # rpm
                self.PrefArray.append(float(x)*math.pi/180) #rad
                self.PrefArray_deg.append(float(x)) # deg
        ref.close()
        
        ## Initial state
        self.state = self.S0_init      
        ## Initial KpW value, %-s/rad
        self.KpW = 0
        ## Initial KpPos value, %/rad
        self.KpPos = 0
        ## Initial reference motor speed, rad/s
        self.Wref = 0
        ## Initial reference motor position, rad
        self.Pref = 0
        ## Initial motor speed, rad/s
        self.Wmeas = 0
        ## Initial motor speed, rpm
        self.Wmeas_rpm = 0
        ## Initial motor position, deg
        self.Pmeas = 0
        ## Initial motor position, rad
        self.Pmeas_rad = 0
        ## Initial time elapsed in closed loop, seconds
        self.t = 0
        ## Initial duty cycle, %
        self.duty = 0        
        ## Initial motor voltage, V
        self.Vm = 0
        
        ## Time array for time elapsed in closed loop, seconds
        self.tArray = [0]
        ## Array containing motor voltage values, V
        self.VmArray = [0]
        ## Array containing measured motor speed values, rpm
        self.WmArray = []
        ## Array containing measured motor position, deg
        self.PmArray = []
        
    def run(self):
        '''
        @brief Runs TaskTrack
        @details Asks user for inputs for speed and position proportional
        gain values. Generates time response plots and error performance
        measurement, J. User can choose to generate another time response or
        end the program.
        '''
        if (self.state == self.S0_init):
            # Run State 0 code
            self.transitionTo(self.S1_KpW)
        
        elif (self.state == self.S1_KpW):
            # Run State 1 code
            self.KpW = input('Enter a value for the speed controller gain, KpW: ')
            if self.testval(self.KpW):
                self.KpW = float(self.KpW)
                print('KpW = {:} %-s/rad'.format(self.KpW))
                self.transitionTo(self.S5_KpPos)
                
        elif (self.state == self.S5_KpPos):
            # Run State 1 code
            self.KpPos = input('Enter a value for the position controller gain, KpPos: ')
            if self.testval(self.KpPos):
                self.KpPos = float(self.KpPos)
                print('KpPos = {:} %/rad'.format(self.KpPos))
                self.transitionTo(self.S2_loop)
                print('Generating plots...')
        
        elif (self.state == self.S2_loop):
            # Run State 2 code
            for n in range(len(self.TrefArray)):
                self.t = self.TrefArray[n]
                self.Wref = self.WrefArray[n] # rad/s
                self.Pref = self.PrefArray[n] # rad
                self.duty = self.Control.update(self.Wref, self.Wmeas, self.Pref, self.Pmeas_rad, self.KpW, self.KpPos)
                self.Vm = (self.Motor.setDuty(self.duty))    
                self.VmArray.append(self.Vm)
                self.tArray.append(self.t)
                ## Transfer function output: motor speed arrray, rad/s
                self.Wout = self.Encoder.speed(self.VmArray, self.tArray)
                self.Wmeas = self.Wout[-1] # rad/s
                self.Wmeas_rpm = self.Wout[-1]*30/math.pi # rpm
                self.WmArray.append(self.Wmeas_rpm) # rpm
                ## Transfer function output: motor position array, rad
                self.Pout = self.Encoder.position(self.VmArray, self.tArray)
                self.Pmeas_rad = self.Pout[-1] # rad
                self.Pmeas = self.Pout[-1]*180/math.pi # deg
                self.PmArray.append(self.Pmeas) # deg
                ## Performance metric for this run
                self.J_k = (self.Wref - self.Wmeas)**2 + (self.Pref - self.Pmeas_rad)**2
                ## Performance metric array
                self.JArray = []
                self.JArray.append(self.J_k)
                #print('t {:0.2f} | Wref {:0.2f} | L {:0.2f} | Vm {:0.2f} | Wm {:0.2f} | Pm {:0.2f}'.format(self.t, self.Wref, self.duty, self.Vm, self.Wmeas, self.Pmeas))
            
            plt.figure(1)
            # Speed
            plt.subplot(2,1,1)
            plt.plot(self.TrefArray,self.WmArray) # Measured speed
            plt.plot(self.TrefArray, self.WrefArray_rpm) # Reference speed
            plt.ylabel('Velocity [RPM]')
            # Position
            plt.subplot(2,1,2)
            plt.plot(self.TrefArray,self.PmArray) # Measured position
            plt.plot(self.TrefArray, self.PrefArray_deg) # Reference position
            plt.xlabel('Time [s]')
            plt.ylabel('Position [deg]')
            plt.show()
            
            ## Performance metric, J
            self.J = np.sum(self.JArray)/len(self.TrefArray)
            print('J = {:}'.format(self.J))
            
            self.transitionTo(self.S3_cont)
        
        elif (self.state == self.S3_cont):
            # Run State 3 code
            ## User input to continue or end program
            self.cont = input('Continue? Enter Y or N: ')
            if self.cont:
                if (self.cont == 'Y'):
                    self.transitionTo(self.S1_KpW)
                    self.Wmeas = 0
                    self.duty = 0
                    self.Vm = 0
                    self.VmArray = [0]
                    self.WmArray = []
                    self.tArray = [0]
                    self.PmArray = []
                elif (self.cont == 'N'):
                    print('Program ended')
                    self.transitionTo(self.S4_end)
                else:
                    print('Invalid input. Please enter "Y" for yes or "N" for no')
                    self.cont = None
                    pass
        
        elif (self.state == self.S4_end):
            # Run State 4 code
            pass

        else:
            # Error state
            pass
        
    def transitionTo(self, newState):
        '''
        @brief Updates next state to run
        @param newState New state to transition to
        '''
        self.state = newState
        
    def testval(self, val):
        '''
        @brief Checks if user input a number
        @details Checks if user input a number, integer or float, for Kp. If a
        number was not entered, function erases the user input and asks the 
        user to input a number. If a number was entered, function returns the 
        number as a string.
        @param val User input
        '''
        try:
            float(val)
        except:
            print('Invalid input: Please enter a number')
            val = None
        else:
            return val
        
class ClosedLoop:
    '''
    @brief Proportional controller
    @details Proportional controller that adjust motor voltage based on error
    between desired motor speed and measured motor speed
    '''
    def __init__(self):
        '''
        @brief Create ClosedLoop object
        '''
        #print('Creating a controller object')
        
    def update(self, Wref, Wmeas, Pref, Pmeas, KpW, KpPos):
        '''
        @brief Returns duty cycle based on the measured and reference values
        @param Wref Reference motor speed, rad/s
        @param Wmeas Measured motor speed, rad/s
        @param Pref Reference motor position, rad
        @param Pmeas Measured motor position, rad
        @param KpW Proportional gain based on motor speed error, %-s/rad
        @param KpPos Proportional gain based on motor position error, %/rad
        '''
        ## Reference motor speed, rad/s
        self.Wref = Wref
        
        ## Measured motor speed, rad/s
        self.Wmeas = Wmeas
        
        ## Reference motor position, rad
        self.Pref = Pref
        
        ## Measured motor position, rad
        self.Pmeas = Pmeas
        
        ## Proportional gain based on speed feedback, %-s/rad
        self.KpW = KpW
        
        ## Proportional gain based on position feedback, %/rad
        self.KpPos = KpPos
        
        ## New duty cycle, %
        duty = self.KpW*(self.Wref - self.Wmeas) + self.KpPos*(self.Pref - self.Pmeas)
        return duty
        
class MotorDriver:
    '''
    @brief Sets motor voltage
    @details Sets voltage sent to virtual motor by adjusting the duty cycle
    '''
    def __init__ (self):
        '''
        @brief Creates MotorDriver object
        '''
        #print('Creating a motor object')
        
        ## Motor nominal DC voltage, V
        self.Vdc = 18
        
        ## Initial motor voltage, V
        self.Vm = 0
        
        ## Initial level, duty cylce
        self.L = 0
        
    def setDuty(self, duty):
        '''
        @brief Sets the duty cycle of motor
        @details This method sets the duty cycle to be sent to the motor to the
        given level. Positive values cause rotation in the forward direction,
        negative values in the reverse direction.
        @param duty Duty cycle of PWM signal sent to motor, %
        '''
        self.L = duty/100
        
        if (self.L >= 1):
            self.Vm = self.Vdc
            return self.Vm
        elif (self.L <= -1):
            self.Vm = -self.Vdc
            return self.Vm
        elif (self.L > -1 and self.L < 1):
            self.Vm = self.L*self.Vdc
            return self.Vm
        else:
            # Error state
            print('Error: setDuty')
        
class EncoderDriver:
    '''
    @brief Simulates encoder behavior
    @details Simulates encoder behavior by "meaasuring" motor speed. Contains
    the motor parameters and uses them in a transfer function that outputs
    motor speed based on voltage supplied to the motor. Positive output for
    speed indicates rotation in the forward direction. Negative output for
    speed indicates rotation in the reverse direction.
    '''
    def __init__(self):
        '''
        @brief Creates EncoderDriver object
        '''
        #print('Creating an Encoder object')
        
        ## Torque constant, N-m/A
        self.Kt = 0.0138
        
        ## Speed constant, V/(rad/s)
        self.Kv = 0.00144
        
        ## Rotational moment of inertia, kg-m2
        self.J = 0.000000522
        
        ## Resistance, Ohm
        self.R = 2.21
        
        ## Viscous damping coefficient, N-m/(rad/s)
        self.b = 0.0000860
    
    def speed(self, U, T):
        '''
        @brief Reads motor speed
        @param U Input array: motor voltage, V
        @param T Input time array, seconds
        '''
        # Create transfer function, input motor voltage & output motor speed
        
        ## Speed transfer function numerator
        self.num = [self.Kt/self.R]
        
        ## Speed transfer function denominator
        self.den = [self.J, self.b + self.Kv*self.Kt/self.R]
        
        ## Speed transfer function
        self.sys = signal.TransferFunction(self.num, self.den)
        
        # Time response
        
        ## Input array: motor voltage
        self.U = U      
        ## Input time array, seconds
        self.T = T
        
        ## Output: time array, motor speed array [rad/s]
        t,y,x = signal.lsim(self.sys, self.U, self.T)
        return y
        
    def position(self, U, T):
        '''
        @brief Reads motor position
        @param U Input array: motor voltage, V
        @param T Input time array, seconds
        '''
        # Create transfer function, input motor voltage & output motor position
        
        ## Position transfer function numerator
        self.num = [self.Kt/self.R]
        
        ## Position transfer function denominator
        self.den = [self.J, self.b + self.Kv*self.Kt/self.R, 0]
        
        ## Position transfer function
        self.sys = signal.TransferFunction(self.num, self.den)
        
        # Time response
        
        ## Input array: motor voltage
        self.U = U        
        ## Input time array, seconds
        self.T = T
        
        ## Output: time array, motor position array [rad], some other array
        t,y,x = signal.lsim(self.sys, self.U, self.T)
        return y
    
# if __name__ == '__main__':
#     # print(Motor.setDuty(50))
#     # print(Encoder.speed())
#     Controller = ClosedLoop()
#     Motor = MotorDriver()
#     Encoder = EncoderDriver()
#     task = TaskTrack(Controller, Motor, Encoder)
#     while True:
#         task.run()