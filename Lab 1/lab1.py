'''@lab2.py'''

# Lab 1: Fibonacci
# Author: Emma Oneto
# Section: ME 305-03
# Date: 9/23/2020

def fib (idx) :
    '''Define "fib" function with input "idx"'''
    print ('Calculating Fibonacci number at ''index n = {:}.'.format(idx))
    '''Tell user that function is calculating the Fibonacci number at their input'''
    if type(idx) != int :
        print('Error: invalid input. Please input a positive integer')
        '''For non-integer input, return error'''
    elif idx == 0:
        print('Fibonacci number is 0.')
        '''For input 0, return 0'''
    elif idx == 1:
        print('Fibonacci number is 1.')
        '''For input 1, return 1'''
    elif idx >= 2:
        fibnum = [0,1]
        '''fibnum will be entire Fib sequence up to idx'''
        n = 2
        while idx >= n:
            fibnum.append(fibnum[n - 1] + fibnum[n - 2])
            n = n + 1
        print('Fibonacci number is {:}.'.format(fibnum[idx]))
        '''Return idx'th Fib no. from fibnum sequence'''
    else :
        print('Error: invalid input. Please input a positive integer')
        '''For all other cases, return error'''

if __name__ == '__main__':
    fib(-5)