'''
@file lab5_user.py
@brief This file contains the TaskUser and BLEDriver for Lab 5
@author Emma Oneto
@date December 4, 2020
'''
# import serial
import lab5_shares as shares
import time
import matplotlib.pyplot as plt

class TaskUser:
    '''
    @brief User interface to control frequency of blinking LED
    @details Asks user for frequency input between 1 - 10 Hz. Rejects user
    inputs that are not numbers. If user inputs a number, sends number to 
    TaskLED. Waits for response from TaskLED, and plots LED brightness, where
    1 means the LED is at maximum brightness, and 0 means the LED is off. Asks
    the user to run another frequency input.
    '''
    ## State 0: Initialization
    S0_init     = 0
    ## State 1: Wait for user input
    S1_wait     = 1
    ## State 2: LED blinking
    S2_blink    = 2
    
    def __init__ (self, BLEDriver, dbg = False):
        '''
        @brief Creates TaskUser object
        '''
        ## Initial state
        self.state = self.S0_init
        
        ## Number of runs of task completed
        self.runs = 0
        
        ## BLEDriver object - to run from BLEDriver class
        self.BLE = BLEDriver
        
        ## Option to print debugging statements as code runs
        self.dbg = dbg
        
        ## Array of elapsed time, seconds
        self.tArray = []
        
        ## Array of LED brightness (0 = off, 1 = on)
        self.LEDArray = []
    
    def run (self):
        '''
        @brief Runs one iteration of TaskUser
        '''
        if (self.state == self.S0_init):
            # Run State 0 code
            self.printTrace()
            self.transitionTo(self.S1_wait)
        
        elif (self.state == self.S1_wait):
            # Run State 1 code
            self.printTrace()
            self.BLE.write('Enter frequency 1 - 10 Hz')
            ## User input
            self.input = self.BLE.read()
            if self.testval(self.input):
                shares.cmd = self.input
                self.BLE.write('LED blinking at {:} Hz'.format(self.input))
                self.input = None
                self.transitionTo(self.S2_blink)
                
        elif (self.state == self.S2_blink):
            if shares.resp:
                self.tArray = shares.resp[0]
                self.LEDArray = shares.resp[1]
                # Plot results
                plt.figure(1)
                plt.plot(self.tArray, self.LEDArray)
                plt.xlabel('Time (s)')
                plt.ylabel('LED Brightness (On/Off)')
                plt.show()
                shares.resp = None
                self.transitionTo(self.S1_wait)
        
        else:
            # Error state
            pass
        
        self.runs += 1
        
    def transitionTo(self, newState):
        '''
        @brief Transitions from current state to new state
        @param newState State to transition to
        '''
        self.state = newState
        
    def testval(self, val):
        '''
        @brief Checks if user input a number
        @details Checks if user input a number, integer or float, for Kp. If a
        number was not entered, function erases the user input and asks the 
        user to input a number. If a number was entered, function returns the 
        number as a string.
        @param val User input
        '''
        try:
            float(val)
        except:
            print('Invalid input: Please enter a number between 1 and 10')
            val = None
        else:
            if (float(val) >= 1 and float(val) <= 10):
                return val
            else:
                print('Invalid input: Please enter a number between 1 and 10')
                val = None
        
    def printTrace(self):
        '''
        @brief Prints a debugging statement
        '''
        if self.dbg:
            print('TaskUser | Run: {:} | State: {:}'.format(self.runs, self.state))
        
class BLEDriver:
    '''
    @brief Reads and writes to and from phone app
    @details Simulates reading and writing to the bluetooth phone app. But I
    can't connect to my Nucleo, it just reads and writes to the Spyder command
    window.
    '''
    def __init__ (self):
        '''
        @brief Creates BLEDriver object
        '''
    
    def write(self, val):
        '''
        @brief Sends strings to app
        @param val String to be sent to app
        '''
        ### Initialize serial port
        #ser = serial.Serial(port = 'COM3', baudrate = 9600, timeout = 1)
        #ser.write(str(val).encode('ascii'))
        #ser.close()
        print(val)
    
    def read (self):
        '''
        @brief Reads strings from app
        '''
        ### Initialize serial port
        #ser = serial.Serial(port = 'COM3', baudrate = 9600, timeout = 1)
        #val = ser.readline().decode('ascii')
        #ser.close()
        val = input()
        return val

if __name__ == '__main__':
    ## BLEDriver object
    BLEDriver = BLEDriver()
    ## TaskUser object
    task1 = TaskUser(BLEDriver)
    while True:
        task1.run()