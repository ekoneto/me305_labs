'''
@file lab5_shares.py
@brief This file contains inter-task variables for Lab 5
@author Emma Oneto
@date December 4, 2020
'''

## User input from TaskUser, string
cmd = None

## Response from TaskLED, matrix (Column 1 = time array, Column 2 = LED brightness array)
resp = None