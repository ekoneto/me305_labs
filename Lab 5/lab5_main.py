'''
@file lab6_main.py
@brief Runs Lab 5 code to blink an LED based on user inputs from an app
@author Emma Oneto
@date December 4, 2020
'''

from lab5_user import TaskUser, BLEDriver
from lab5_led import TaskLED, LEDDriver

## BLEDriver object
BLEDriver = BLEDriver()

## LEDDriver object
LEDDriver = LEDDriver()

## TaskUser object
task1 = TaskUser(BLEDriver, dbg = False)

## TaskLED object
task2 = TaskLED(LEDDriver, dbg = False)

while True:    
    task2.run()
    task1.run()