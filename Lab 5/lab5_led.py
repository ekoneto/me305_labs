'''
@file lab5_led.py
@brief This file contains the TaskLED and LEDDriver for Lab 5.
@author Emma Oneto
@date December 4, 2020
'''
import lab5_shares as shares
import time

class TaskLED:
    '''
    @brief Blinks virtual LED at a frequency input from TaskUser
    @details Upon initialization, turns LED off. Waits for input from TaskUser.
    Blinks LED at the frequency input for 2 seconds. Sends time array and LED
    brightness array to TaskUser. In LED brightness array, 1 means the LED is
    on, and 2 means the LED is off.
    '''
    ## State 0: Initialization
    S0_init = 0
    ## State 1: Wait for user input
    S1_wait = 1
    ## State 2: LED On
    S2_on   = 2
    ## State 3: LED Off
    S3_off  = 3
    
    def __init__(self, LEDDriver, dbg = False):
        '''
        @brief Creates TaskLED object
        '''
        ## Initial state
        self.state = self.S0_init
        
        ## Number of runs of task completed
        self.runs = 0
        
        ## LEDDriver object - to run from LEDDriver class
        self.LED = LEDDriver
        
        ## Array of elapsed time, seconds
        self.tArray = []
        
        ## Array of LED brightness (0 = off, 1 = on)
        self.LEDArray = []
        
        ## Option to print debugging statements as code runs
        self.dbg = dbg
    
    def run (self):
        '''
        @brief Runs one iteration of task
        '''
        if (self.state == self.S0_init):
            # Run State 0 code
            self.printTrace()
            self.LED.off()
            self.transitionTo(self.S1_wait)
        
        elif (self.state == self.S1_wait):
            # Run State 1 code
            self.printTrace()
            #shares.cmd = input()
            if shares.cmd:
                ## LED blinking frequency, Hz
                self.f = float(shares.cmd)
                if self.dbg:
                    print('Frequency: {:} Hz'.format(self.f))
                ## LED blinking period, seconds
                self.T = 1/self.f
                if self.dbg:
                    print('Period: {:} s'.format(self.T))
                shares.cmd = None
                self.transitionTo(self.S2_on)
                ## Start time, seconds
                self.tstart = time.time()
                ## Time when LED is turned on, seconds
                self.tstarton = time.time()
                ## Time at this frequency, seconds
                self.t = 0
                
        elif (self.state == self.S2_on):
            # Run State 2 code
            self.printTrace()
            self.t = time.time() - self.tstart
            if self.dbg:
                print(self.t)
            ## Time elapsed since LED turned on/off, seconds
            self.telapse = time.time() - self.tstarton
            if (self.t <= 2):
                if (self.telapse <= self.T/2):
                    self.tArray.append(self.t)
                    self.LEDArray.append(self.LED.on())
                elif (self.telapse >= self.T/2):
                    self.transitionTo(self.S3_off)
                    ## Time when LED is turned off, seconds
                    self.tstartoff = time.time()
            elif (self.t >= 2):
                shares.resp = [self.tArray, self.LEDArray]
                # Reset values
                self.f = None
                self.T = None
                self.tArray = []
                self.LEDArray = []
                self.transitionTo(self.S1_wait)
                self.LED.off()
                
        elif (self.state == self.S3_off):
            # Run State 3 code
            self.printTrace()
            self.t = time.time() - self.tstart
            if self.dbg:
                print(self.t)
            self.telapse = time.time() - self.tstartoff
            if (self.t <= 2):
                if (self.telapse <= self.T/2):
                    self.tArray.append(self.t)
                    self.LEDArray.append(self.LED.off())
                elif (self.telapse >= self.T/2):
                    self.transitionTo(self.S2_on)
                    self.tstarton = time.time()
            elif (self.t >= 2):
                shares.resp = [self.tArray, self.LEDArray]
                # Reset values
                self.f = None
                self.T = None
                self.tArray = []
                self.LEDArray = []
                self.transitionTo(self.S1_wait)
                self.LED.off()
                
        else:
            # Error State
            pass
                
        self.runs += 1
        
    def transitionTo(self, newState):
        '''
        @brief Transitions from current state to new state
        @param newState State to transition to
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        @brief Prints a debugging statement
        '''
        if self.dbg:
            print('TaskLED | Run: {:} | State: {:}'.format(self.runs, self.state))
        
class LEDDriver:
    '''
    @brief Simulates LED
    @details Turns a virtual LED on and off. Returns a 1 for LED on and a 0
    LED off.
    '''
    def __init__ (self):
        '''
        @brief Creates LEDDriver object
        '''
        
    def on (self):
        '''
        @brief Turns LED on to maximum brightness
        '''
        #print('LED On')
        val = 1
        return val
        
    def off (self):
        '''
        @brief Turns LED off
        '''
        #print('LED Off')
        val = 0
        return val
        
if __name__ == '__main__':
    ## LEDDriver object
    LEDDriver = LEDDriver()
    ## TaskLED object
    task2 = TaskLED(LEDDriver)
    while True:
        task2.run()